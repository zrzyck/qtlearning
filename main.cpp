#include "pomocnikbiurowy.h"
#include <QApplication>

struct dbfHeader
{
    unsigned int dbfVersion : 8;
    unsigned int lastEditDate : 3*8; // formatted as YYMMDD
    unsigned int numberOfRecords : 32;
    unsigned int headerSize : 16;
    unsigned int recordSize : 16;
    unsigned int reserved : 16; //should be filled with zeros
    unsigned int incompleteTransactionFlag : 8;
    unsigned int encryptionFlag : 8;
};


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PomocnikBiurowy w;
    w.show();

    return a.exec();
}
