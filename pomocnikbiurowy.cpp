#include "pomocnikbiurowy.h"
#include "ui_pomocnikbiurowy.h"

#include <QSettings>
#include <QMessageBox>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include "xlsxdocument.h"
#include "xlsxchartsheet.h"
#include "xlsxcellrange.h"
#include "xlsxchart.h"
#include "xlsxrichstring.h"
#include "xlsxworkbook.h"
using namespace QXlsx;



PomocnikBiurowy::PomocnikBiurowy(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PomocnikBiurowy)
{
    ui->setupUi(this);
    loadSettings();
}

PomocnikBiurowy::~PomocnikBiurowy()
{
    saveSettings();
    delete ui;
}

void PomocnikBiurowy::saveSettings()
{
    QSettings settings("Pawel", "Zarzycki");
    settings.beginGroup("MainWindow");
    settings.setValue("position",this->geometry());
    settings.setValue("lineEditDbfTemplate",ui->lineEditDbfTemplate->text());
    settings.setValue("lineEditSourceFile",ui->lineEditSourceFile->text());
    settings.setValue("lineEditGenDbfDatabase",ui->lineEditGenDbfDatabase->text());
    settings.endGroup();
}

void PomocnikBiurowy::loadSettings()
{
    QSettings settings("Pawel", "Zarzycki");
    settings.beginGroup("MainWindow");
    setGeometry(settings.value("position").toRect());
    ui->lineEditDbfTemplate->setText(settings.value("lineEditDbfTemplate").toString());
    ui->lineEditSourceFile->setText(settings.value("lineEditSourceFile").toString());
    ui->lineEditGenDbfDatabase->setText(settings.value("lineEditGenDbfDatabase").toString());
    settings.endGroup();
}

void PomocnikBiurowy::fillArray(char *array,size_t size,char value)
{
    for (size_t x = 0 ; x <= size ; x++)
    {
        array[x] = value;
    }
}


/* Select DBF TEMPLATE

  Template should be extracted to some QByteArray (cause i'm not sure how to extract it correctly to byte[])
  Than some header info should be extracted, like header size & number of field descriptors.
  Some field descryptor translator should be created -> in DBF there are only 3 asci fields i donno what they mean.
*/
void PomocnikBiurowy::on_buttonSelectInputFile_clicked()
{
    QString inputFileName = QFileDialog::getOpenFileName(this,"Wybierz plik do odczytu", QDir::homePath());
    QFile inputFile(inputFileName);

    if (!inputFile.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this,"Ostrzeżenie","Plik nie został otwarty");
    }
    else
    {
        ui->lineEditDbfTemplate->setText(inputFileName);
        QTextStream in (&inputFile);
        QByteArray inArray = inputFile.read(8000);
        QString text = in.readLine();
        ui->plainTextEdit->setPlainText(text);

        const char *ptr = text.toLocal8Bit();
        QString tempText = static_cast<QString>(*ptr) + static_cast<QString>(*(ptr+1));

        uint16_t headerSize = static_cast<uint16_t>(static_cast<uint16_t>(inArray[10]) + (inArray[11] << 8));
        uint8_t* header = new uint8_t[headerSize];
        *header = *reinterpret_cast<uint8_t*>(in.readLine().data_ptr()); //there is something wrong with it.

        ui->plainTextEdit->setPlainText( QString::number(headerSize,10));
        //ui->plainTextEdit->setPlainText(QString::number(header[11]));

        inputFile.close();
        delete[] header;
    }
}

/* Select source file from commarch. It gonna be some exel file.

*/
void PomocnikBiurowy::on_butttonSelectSourceFile_clicked()
{
    QString inputFileName = QFileDialog::getOpenFileName(this,"Wybierz plik do odczytu", QDir::homePath());
    QFile inputFile(inputFileName);

    if (!inputFile.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this,"Ostrzeżenie","Plik nie został otwarty");
    }
    else
    {
        ui->lineEditSourceFile->setText(inputFileName);
    }
    inputFile.close();
}

void PomocnikBiurowy::on_butttonSelectOutputFile_clicked()
{

}
/* Generate DBF

    Use some magic library to read exel file.
    Create some table[X][Y]. Where X is number of documents listed in exel and Y is 32 bytes * number of field descriptors from dbf.
    Create new file & start filling it with new header
    Fill file with the records. (how?!)

 */
void PomocnikBiurowy::on_buttonGenNewFile_clicked()
{
    //QString("c:\\Users\\pawel.zarzycki\\Downloads\\ola_ss.xls");
    QXlsx::Document xlsx("c:\\Users\\pawel.zarzycki\\Downloads\\ola_ss2.xlsx");
    if (xlsx.load())
    {
        Cell* cell;
        QVariant readValue;
        int docNameColumnNumber = 0;
        int docDateColumnNumber = 0;
        int nettoColumnNumber = 0;
        int bruttoColumnNumber = 0;

        for (int column = 1; column<=30; column++)
        {
            cell = xlsx.cellAt(1,column);
            if (cell != nullptr)
            {
                readValue = cell->readValue();
                if("Netto" == readValue.toString())
                {
                    nettoColumnNumber = column;
                }
                if("Brutto" == readValue.toString())
                {
                    bruttoColumnNumber = column;
                }
                if("Data wyst." == readValue.toString())
                {
                    docDateColumnNumber = column;
                }
                if("Numer dokumentu" == readValue.toString())
                {
                    docNameColumnNumber = column;
                }
            }
        }


        int exelRecords = 1;
        cell = xlsx.cellAt(1, 1);
        for ( ; cell != nullptr ;)
        {
            cell = xlsx.cellAt(exelRecords,docNameColumnNumber);
            readValue = cell->readValue();
            ui->plainTextEdit->insertPlainText("\n" + readValue.toString() + "   ");
            cell = xlsx.cellAt(exelRecords,docDateColumnNumber);
            readValue = cell->readValue();
            ui->plainTextEdit->insertPlainText("\t" + readValue.toString());
            cell = xlsx.cellAt(exelRecords,nettoColumnNumber);
            readValue = cell->readValue();
            ui->plainTextEdit->insertPlainText("\t" + readValue.toString());
            cell = xlsx.cellAt(exelRecords,bruttoColumnNumber);
            readValue = cell->readValue();
            ui->plainTextEdit->insertPlainText("\t" + readValue.toString());

            exelRecords++;
            cell = xlsx.cellAt(exelRecords,1);
        }
        exelRecords -= 2;
    }

}

    //Create some class with fields like header and record.
    //Open stream to file
    //Write to file header
    //Write to file each record in some for loop.
    //Dont need any big table with all records, just read them from exel, one by one and than write to new file.
    //Close file
    //Clean buchalter import folder and move outputfile there.
