#ifndef POMOCNIKBIUROWY_H
#define POMOCNIKBIUROWY_H

#include <QMainWindow>

namespace Ui {
class PomocnikBiurowy;
}

class PomocnikBiurowy : public QMainWindow
{
    Q_OBJECT

public:
    struct record
    {
        //pusto w C to 0x20
        //pusto w N to 0x00
        char STV = 'N';           //C 1
        char STM = '1';           //C 1
        char ANU = ' ';           //C 1
        char TYV = 0x00;           //N 1 0
        char NRV[30];        //C 30
        char DAV[6] = {1,1,1,1,1,1};        //D (date YYMMDD)
        char KOT[18];       //C 18
        char CET = ' ';           //C 1
        char KON[5];        //N 5 0
        char TYW = 0x00;           //N 1 0
        char TYZ = 0x00;           //N 1 0
        char TYS = 0x00;           //N 1 0
        char NA1[40];       //C 40
        char NA2[40];       //C 40
        char NA3[40];       //C 40
        char NA4[40];       //C 40
        char NA5[40];       //C 40
        char NA6[40];       //C 40
        char KWC[14];       //C 14
        char JED[6];        //C 6
        char ILO[13];       //N 13 4
        char CEN[12];       //N 12 4
        char CEB[12];       //N 12 4
        char WAN[11];       //N 11 2
        char VAT[2];        //N 2 0
        char VAS[4];        //N 4 1
        char WAV[11];       //N 11 2
        char WAB[11];       //N 11 2
        char CZO = '1';           //C 1
        char REK[7];        //N 7 0
        char REC[7];        //N 7 0
        char ILK[13];       //N 13 4
        char KRN[13];       //N 11 2
        char KRV[13];       //N 11 2
        char ZRP = 0x00;           //N 1 0
        char STA = ' ';           //C 1
        char WAL[3];        //C 3
        char KVA[10];       //N 10 6
        char KKS[10];       //N 10 6
        char KILO[13];      //N 13 4
        char KCEN[12];      //N 12 4
        char KCEB[12];      //N 12 4
        char KWNE[11];      //N 11 2
        char KVAT[2];       //N 2 0
        char KVAS[4];       //N 4 1
        char KWAV[11];      //N 11 2
        char KWAB[11];      //N 11 2
        char PAL = ' ';           //C 1
        char AKC = ' ';           //C 1
        char AKK[20];       //C 20
        char VOS[4];        //N 4 1
        char KVO[11];       //N 11 2
        char INF[32];       //C 32
        char RE1[30];       //C 30
        char RE2[11];       //N 11 2
        char RE3 = 'N';           //C 1
        char RE4 = ' ';           //C 1
    };




    explicit PomocnikBiurowy(QWidget *parent = nullptr);
    ~PomocnikBiurowy();

private slots:
    void on_buttonSelectInputFile_clicked();

    void on_butttonSelectSourceFile_clicked();

    void on_butttonSelectOutputFile_clicked();

    void on_buttonGenNewFile_clicked();

private:
    Ui::PomocnikBiurowy *ui;
    void saveSettings();
    void loadSettings();
    void fillArray(char*,size_t,char);
};

#endif // POMOCNIKBIUROWY_H
